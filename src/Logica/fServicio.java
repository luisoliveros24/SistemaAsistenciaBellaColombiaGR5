/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Conexiones.MySQLConection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Usuario
 */
public class fServicio {
    MySQLConection connn = new MySQLConection();
    
    private String sSQL = "";
    private String sSQL2 = "";
    private String sSQL3 = "";
    private int costo1;
    private int costo2;
    private int costo3;
    public static int costoTotal;
    

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;
        
        String[] titulos = {"Nombre Servicio", "Horario Apertura", "Horario Cierre"};
        String[] registro = new String[3];
        
        modelo = new DefaultTableModel(null, titulos);
        sSQL = "SELECT nombreServicio, horaAbreServicio, horaCierraServicio FROM Servicio";
        try {
            Connection conn = connn.setConeccion();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("nombreServicio");
                registro[1] = rs.getString("horaAbreServicio");
                registro[2] = rs.getString("horaCierraServicio");
                
                modelo.addRow(registro);
            }
            return modelo;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;
        }
    }
    
     public DefaultTableModel mostrar1(String buscar) {
        DefaultTableModel modelo;
        
        String[] titulos = {"Nombre Servicio", "Horario Apertura", "Horario Cierre", "Precio"};
        String[] registro = new String[4];
        
        modelo = new DefaultTableModel(null, titulos);
        sSQL = "SELECT nombreServicio, horaAbreServicio, horaCierraServicio, precioServicio FROM Servicio Where TipoSevicio_idTipoSevicio=3";
        try {
            Connection conn = connn.setConeccion();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("nombreServicio");
                registro[1] = rs.getString("horaAbreServicio");
                registro[2] = rs.getString("horaCierraServicio");
                registro[3] = rs.getString("precioServicio");
                
                modelo.addRow(registro);
            }
            return modelo;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;
        }
    }
     
    public int calcularPrecioTotal(){
    
    sSQL="select sum(precioServicio) from servicio where tiposevicio_idtiposevicio=3";
    sSQL2="select sum(costopeaje) from peaje WHERE ruta_idruta=3;";
    sSQL3="select sum(costogasolinavehiculo) from tipovehiculo where idtipovehiculo=1";
    
    
    
    try {
            Connection conn = connn.setConeccion();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            Connection conn1 = connn.setConeccion();
            Statement st1 = conn1.createStatement();
            ResultSet rs1 = st1.executeQuery(sSQL2);
            Connection conn2 = connn.setConeccion();
            Statement st2 = conn2.createStatement();
            ResultSet rs2 = st2.executeQuery(sSQL3);

             while (rs.next()) {
           costo1 = rs.getInt("sum(precioServicio)");
                   }
              while (rs1.next()) {
            costo2 = rs1.getInt("sum(costopeaje)");
                    }
               while (rs2.next()) {
              costo3 = rs2.getInt("sum(costogasolinavehiculo)");
    }
               costoTotal = costo1+costo2+costo3;
           
           
            
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            
        }
        return costoTotal;
    

    
    }
        
    
    
}
