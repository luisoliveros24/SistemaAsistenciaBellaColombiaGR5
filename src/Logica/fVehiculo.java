/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Conexiones.MySQLConection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Usuario
 */
public class fVehiculo {
    
    MySQLConection connn = new MySQLConection();
    
    private String sSQL = "";
    
    public DefaultTableModel mostrarV(String buscar) {
        DefaultTableModel modelo;
        
        String[] titulos = {"Tipo de Vehiculo", "Precio Gasolina", };
        String[] registro = new String[2];
        
        modelo = new DefaultTableModel(null, titulos);
        sSQL = "SELECT nombreTipoVehiculo, costoGasolinaVehiculo FROM tipovehiculo WHERE idTipoVehiculo=1";
        try {
            Connection conn = connn.setConeccion();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("nombreTipoVehiculo");
                registro[1] = rs.getString("costoGasolinaVehiculo");
                
                modelo.addRow(registro);
            }
            return modelo;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;
        }
    }
    
    
}
