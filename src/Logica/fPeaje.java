/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Conexiones.MySQLConection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Usuario
 */
public class fPeaje {
    
    MySQLConection connn = new MySQLConection();
    
    private String sSQL = "";
    
    public DefaultTableModel mostrarP(String buscar) {
        DefaultTableModel modelo;
        
        String[] titulos = {"Nombre Peaje", "Precio", };
        String[] registro = new String[2];
        
        modelo = new DefaultTableModel(null, titulos);
        sSQL = "SELECT nombrePeaje, costoPeaje FROM Peaje WHERE ruta_idruta=3";
        try {
            Connection conn = connn.setConeccion();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("nombrePeaje");
                registro[1] = rs.getString("costoPeaje");
                
                modelo.addRow(registro);
            }
            return modelo;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;
        }
    }
    
}
